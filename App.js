import React from 'react';
import { createStackNavigator, createAppContainer } from 'react-navigation'; // Version can be specified in package.json
import { Provider } from 'react-redux';
import configureStore from "./store";
const store = configureStore();
import { connect } from 'react-redux';
import { auth } from './src/redux/action/AuthAction';
import HomeScreen from './src/component/HomeScreen';
import LoginScreen from './src/component/LoginScreen';
import SignUpScreen from './src/component/SignUpScreen';
import ToDoListScreen from './src/component/ToDoListScreen';
import AddToDoListScreen from './src/component/AddToDoListScreen';

const RootStack = createStackNavigator(
  {
    Home: HomeScreen,
    Login : {
      screen: LoginScreen
    },
    ToDoList: {
      screen: ToDoListScreen
    },
    SignUp: {
      screen: SignUpScreen
    },
    AddToDoList: {
      screen: AddToDoListScreen
    },
  },
  {
    initialRouteName: 'Home',
    headerMode: 'none',
  }
);

const AppContainer = createAppContainer(RootStack);

export default class App extends React.Component {
  render() {
    return (
    <Provider store={store}>
       <AppContainer />
    </Provider>
    )
  }
}