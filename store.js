import { createStore } from 'redux'
import rootReducer from './src/redux/reducer/index';
// import createSagaMiddleware from 'redux-saga';
// import IndexSaga from './src/redux/saga/index';

const configureStore = () => {
    return {
        ...createStore(rootReducer)
    };
};

export default configureStore;