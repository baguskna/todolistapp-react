import React, { Component } from 'react'
import { View, ToastAndroid, StyleSheet, Dimensions, ActivityIndicator } from 'react-native'
import axios from 'axios'
import { connect } from 'react-redux'
import { auth } from '../redux/action/AuthAction'
import AsyncStorage from '@react-native-community/async-storage';
import { Item, Button, Container, Header, Left, Body, Title, Icon, Label, Input, Text, TouchableOpacity, Content, Card, CardItem, Right, Form, Spinner } from 'native-base';
import { withNavigation } from 'react-navigation';

class LoginScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      username: '',
      password: '',
      isLoading: false
    }
  }

  login = () => {
    this.setState({ isLoading: true })
    const postDataLogin = async (bodyParameter) => await axios.post(
      `https://app-todoapps.herokuapp.com/users/login`, bodyParameter, {
      headers: {
        'Content-Type': 'application/json'
      }
    }
    )
    postDataLogin({
      username: 'khairunnishafifa',
      password: 'nasha1898'
    })
      .then(response => {
        this.props.FetchToken(response.data.result)
        AsyncStorage.setItem('@token', response.data.result)
        this.setState({
          username: '',
          password: '',
          isLoading: false
        })
        ToastAndroid.show('Login success', ToastAndroid.SHORT)
        this.props.navigation.navigate('ToDoList')
      })
      .catch(() => {
        ToastAndroid.show('Login failed', ToastAndroid.SHORT)
        this.setState({
          isLoading: false
        })
      })
  }

  render() {
    return (
      <Container>


        <Header style={{ backgroundColor: '#00acee' }}
          androidStatusBarColor="#00acee">

          <Left>
            <Button transparent
              onPress={() => { this.props.navigation.goBack() }}>
              <Icon name='arrow-back' />
            </Button>
          </Left>

          <Body>
            <Title style={styles.titleheader}>Sign In ToDoList.</Title>
          </Body>

          <Right>
            <Button transparent
              onPress={() => this.props.navigation.navigate('SignUp')}>
              <Text style={styles.titleheader}>Sign Up</Text>
            </Button>
          </Right>

        </Header>


        <View style={{ flex: 1, alignItems: 'center', marginTop: 40 }}>

          <Item
            floatingLabel
            style={{
              width: Dimensions.get('window').width * 0.9
            }}>
            <Label>
              <Text>Username</Text>
            </Label>
            <Input value={this.state.username} onChangeText={(text) => this.setState({ username: text })} />
          </Item>


          <Item
            floatingLabel
            style={{
              width: Dimensions.get('window').width * 0.9, marginTop: 15
            }}>
            <Label>
              <Text>Password</Text>
            </Label>
            <Input secureTextEntry={true} value={this.state.password} onChangeText={(text) => this.setState({ password: text })} />
          </Item>

          <Button
            style={{
              width: Dimensions.get('window').width * 0.9,
              backgroundColor: 'orange',
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 30,
              borderRadius: 100 / 12
            }}
            onPress={() => this.login()}>

            {
              this.state.isLoading ?
              (<View>
                <ActivityIndicator animating={this.state.isLoading} size="small" color="#ffffff" />
              </View>)
                :
                (<View>
                  <Text style={styles.text}>Sign In</Text>
                </View>)
            }
            
          </Button>

          <Text style={{ marginTop: 30 }}>Forgot Password?</Text>

        </View>


      </Container>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth
});

const mapDispatchToProps = dispatch => {
  return {
    FetchToken: token => dispatch(auth(token))
  }
};

export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(LoginScreen));

const styles = StyleSheet.create({
  titleheader: {
    fontWeight: 'bold',
    alignItems: 'center',
    justifyContent: 'center',
  },
  signbutton: {
    fontWeight: 'bold'
  },
  button: {
    display: "flex",
    borderColor: 'transparent',
    margin: 10,
    justifyContent: "center",
    width: 2000,
  },
  text: {
    color: '#ffffff',
    fontSize: 20,
    justifyContent: "center",
    alignContent: "center",
    fontWeight: 'bold'
  },
  buttonsignin: {
    margin: 10,
  }
})