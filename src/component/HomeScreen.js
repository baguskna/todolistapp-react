import React, { Component } from 'react';
import { Item, Button, Container, Header, Body, Title, Text } from 'native-base';
import { View, StyleSheet } from 'react-native';
import { withNavigation } from 'react-navigation';

class HomeScreen extends Component {
  render() {
    return (
      <Container>
        <Header style={{ backgroundColor: '#00acee' }}
          androidStatusBarColor="#00acee">
          <Body>
            <Title style={styles.titleheader}>ToDoList</Title>
          </Body>
        </Header>
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <Text style={{ fontSize: 35, fontWeight: 'bold' }}>See what do you want</Text>
          <Text style={{ fontSize: 35, fontWeight: 'bold' }}>to do In App now.</Text>
          <Button transparent
            onPress={() => this.props.navigation.navigate('SignUp')}>
            <Text style={styles.text}>Create New Account</Text>
          </Button>
          <Item style={styles.button}>
            <Button transparent
              onPress={() => this.props.navigation.navigate('Login')} >
              <Text style={styles.text}>Login</Text>
            </Button>
          </Item>
        </View>
      </Container>
    );
  }
}

export default withNavigation(HomeScreen)

const styles = StyleSheet.create({
  titleheader: {
    fontWeight: 'bold',
    alignItems: 'center',
    justifyContent: 'center',
  },
  signbutton: {
    fontWeight: 'bold'
  },
  button: {
    display: "flex",
    borderColor: 'transparent',
    margin: 10,
    justifyContent: "center",
    width: 2000,
  },
  text: {
    color: '#00acee',
    fontSize: 20,
    justifyContent: "center",
    alignContent: "center",
    fontWeight: 'bold'
  },
  buttonsignin: {
    margin: 10,
  }
})