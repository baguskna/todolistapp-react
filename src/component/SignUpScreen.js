import React, { Component } from 'react'
import { View, StyleSheet, ToastAndroid, ActivityIndicator } from 'react-native';
import { Item, Button, Container, Header, Left, Body, Title, Icon, Label, Input, Text, TouchableOpacity, Content, Card, CardItem, Right } from 'native-base';
import { createStackNavigator, createAppContainer, withNavigation } from 'react-navigation'; // Version can be specified in package.json
import axios from 'axios';

class SignUpScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      email: '',
      password: '',
      firstName: '',
      lastName: '',
      gender: '',
      isLoading: false
    };
  }

  signUp = () => {
    try {
      this.setState({ isLoading: true })
      const signUpData = async (bodyParameter) => await axios.post(
        'http://basic-todoapi.herokuapp.com/api/user/create', bodyParameter, {
        headers: {
          'Content-Type': 'application/json'
        }
      })
      signUpData({
        username: this.state.username,
        email: this.state.email,
        password: this.state.password,
        firstName: this.state.firstName,
        lastName: this.state.lastName,
        gender: this.state.gender
      })
        .then(() => {
          ToastAndroid.show('User Registration Success', ToastAndroid.SHORT),
            this.props.navigation('Login')
          this.setState({ isLoading: false })
        })
        .catch(() =>
          ToastAndroid.show('User Registration Failed', ToastAndroid.SHORT),
        )
    }
    catch (e) {
    }
  }

  render() {
    return (
      <Container>


        <Header style={{ backgroundColor: '#00acee' }}
          androidStatusBarColor="#00acee">
          <Left>
            <Button transparent
              onPress={() => { this.props.navigation.goBack() }}>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title style={styles.titleheader}>Sign Up To ToDoList.</Title>
          </Body>
        </Header>


        <View
          style={{
            flex: 0.75,
            alignItems: 'center',
            justifyContent: 'center'
          }}
        >

          <Item
            floatingLabel
            style={{
              width: Dimensions.get('window').width * 0.9
            }}>
            >
            <Label style={{ color: 'black' }}>Username</Label>
            <Input
              value={this.state.username}
              onChangeText={(text) => this.setState({ username: text })} />
          </Item>

          <Item
            floatingLabel
            style={{
              width: Dimensions.get('window').width * 0.9
            }}
          >
            <Label style={{ color: 'black' }}>Password</Label>
            <Input
              placeholder="Password"
              secureTextEntry={true}
              value={this.state.password}
              onChangeText={(text) => this.setState({ password: text })} />
          </Item>

          <Item
            floatingLabel
            style={{
              width: Dimensions.get('window').width * 0.9
            }}
          >
            <Label style={{ color: 'black' }}>First Name</Label>
            <Input
              placeholder="First Name"
              value={this.state.firstName}
              onChangeText={(text) => this.setState({ firstName: text })} />
          </Item>

          <Item
            floatingLabel
            style={{
              width: Dimensions.get('window').width * 0.9
            }}
          >
            <Label style={{ color: 'black' }}>Last Name</Label>
            <Input
              placeholder="Last Name"
              value={this.state.lastName}
              onChangeText={(text) => this.setState({ lastName: text })} />
          </Item>

          <Item
            floatingLabel
            style={{
              width: Dimensions.get('window').width * 0.9
            }}
          >
            <Label style={{ color: 'black' }}>Email Address</Label>
            <Input
              placeholder="email"
              keyboardType="email-address"
              value={this.state.email}
              onChangeText={(text) => this.setState({ email: text })} />
          </Item>

          <Item
            floatingLabel
            style={{
              width: Dimensions.get('window').width * 0.9
            }}
          >
            <Label style={{ color: 'black' }}>Gender</Label>
            <Input
              placeholder="Gender"
              value={this.state.gender}
              onChangeText={(text) => this.setState({ gender: text })}
            />
          </Item>

          <View style={{ margin: 10 }}>
            <Button
              style={{
                width: Dimensions.get('window').width * 0.9,
                backgroundColor: 'orange',
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: 30,
                borderRadius: 100 / 12
              }}
              onPress={() => this.signUp()}>

              {
                this.state.isLoading ?
                  (<View>
                    <ActivityIndicator animating={this.state.isLoading} size="small" color="#ffffff" />
                  </View>)
                  :
                  (<View>
                    <Text style={styles.text}>Sign Up</Text>
                  </View>)
              }

            </Button>
          </View>


          <Text>Already have an account?</Text>

          <Button
            transparent
            style={{ color: 'transparent' }}
            onPress={() => this.props.navigation.navigate('Login')}>
            <Text style={styles.signbutton}>Sign In Here</Text>
          </Button>

        </View>


      </Container>
    )
  }
}

const styles = StyleSheet.create({
  titleheader: {
    fontWeight: 'bold',
    alignItems: 'center',
    justifyContent: 'center',
  },
  signbutton: {
    fontWeight: 'bold'
  },
  button: {
    display: "flex",
    borderColor: 'transparent',
    margin: 10,
    justifyContent: "center",
    width: 2000,
  },
  text: {
    color: '#00acee',
    fontSize: 20,
    justifyContent: "center",
    alignContent: "center",
    fontWeight: 'bold'
  },
  buttonsignin: {
    margin: 10,
  }
})

export default withNavigation(SignUpScreen);