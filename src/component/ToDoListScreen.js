import React, { Component } from 'react'
import { View, StyleSheet, ToastAndroid, Text, Dimensions } from 'react-native';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import { Item, Button, Container, Header, Left, Body, Title, Icon, Label, Input, TouchableOpacity, Content, Card, CardItem, Right, Fab } from 'native-base';
import { createStackNavigator, createAppContainer, withNavigation } from 'react-navigation';


class ToDoListScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: [],
      isLoadingUpdate: false
    }
  }

  componentDidMount() {
    this.getData()
    setInterval(this.getData, 100)
  }

  getData = async () => {
    const token = await AsyncStorage.getItem('@token')
    const getDataToDo = async () => await axios.get(
      'https://app-todoapps.herokuapp.com/todos', {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      }
    }
    )
    getDataToDo()
      .then(result => {
        this.setState({
          data: result.data.result
        })
      })
  }

  delToDo = async (id) => {
    try {
      const token = await AsyncStorage.getItem('@token')
      const delDataToDo = async (idd) => await axios.delete(
        `https://app-todoapps.herokuapp.com/todos/${idd}`, {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
        }
      }
      )
      delDataToDo(id)
        .then(res => {
          this.getData()
          ToastAndroid.show("Data Successfully Deleted", ToastAndroid.SHORT)
        })
        .cath(e => {
          ToastAndroid.show("Can't Delete Data", ToastAndroid.SHORT)
        })
    }
    catch (e) {
    }
  }


  updateData = async (id) => {
    try {
      this.setState({ isLoadingUpdate: true })
      const token = await AsyncStorage.getItem('@token')
      const updateDataToDo = async (idd, objParam) => await axios.put(
        `https://app-todoapps.herokuapp.com/todos/${idd}`, objParam, {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
        }
      })

      updateDataToDo(id, { status: true })
        .then(() => {
          ToastAndroid.show(`Update Data Success`, ToastAndroid.SHORT)
          this.getData()
          this.setState({ isLoadingUpdate: false })
        })
        .catch(() => {
          ToastAndroid.show("Data Can't be Updated", ToastAndroid.SHORT)
          this.setState({ isLoadingUpdate: false })
        })
    }
    catch (e) {
    }
  }

  updateDataCanceled = async (id) => {
    try {
      const token = await AsyncStorage.getItem('@token')
      const updateDataToDo = async (idd, objParam) => await axios.put(
        `https://app-todoapps.herokuapp.com/todos/${idd}`, objParam, {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
        }
      })

      updateDataToDo(id, { status: false })
        .then(() => {
          ToastAndroid.show(`Update Data Success`, ToastAndroid.SHORT)
          this.getData()
        })
        .catch(() => {
          ToastAndroid.show("Data Can't be Updated", ToastAndroid.SHORT)
        })
    }
    catch (e) {
    }
  }

  render() {
    const loops = this.state.data.map(todo => {
      const status = todo.status
      const newDate = new Date(todo.dueDate);
      const dateDay = newDate.toDateString().substring(0, 3);
      const dateDayDate = newDate.toDateString().substring(8, 10);
      const dateMonth = newDate.toDateString().substring(4, 7);
      const dateYear = newDate.toDateString().substring(11, 15);

      return (
        <View
          key={todo._id}
          style={{
            alignItems: 'center',
            justifyContent: 'center'
          }}
        >

          <Card
            style={{
              marginVertical: 15,
              width: Dimensions.get('window').width * 0.95,
              borderRadius: 100 / 6
            }}
          >

            <CardItem
              style={{
                borderRadius: 100 / 6
              }}>

              <Body>
                <Text
                  style={{
                    fontSize: 20,
                    fontFamily: 'Roboto',
                    fontWeight: 'bold',
                    color: '#015249',
                    marginVertical: 5
                  }}
                >{todo.title}</Text>
                <Text>Note: {todo.notes}</Text>
                <Text>Priority; {todo.priority}</Text>
                {/* <Text>{dateDay + ', ' + dateDayDate + ' ' + dateMonth + ' ' + dateYear}</Text> */}

                {
                  status == true ?
                    (<View>
                      <Text>Todo finished</Text>
                    </View>)

                    :

                    (<View>
                      <Text>Todo unfinished</Text>
                    </View>)
                }

                <View style={{ flex: 1, flexDirection: 'row', marginVertical: 5 }}>
                  <Left>
                    <Button
                      transparent
                      onPress={() => this.delToDo(todo._id)}>
                      <Icon style={{ color: "red" }} name="trash" />
                    </Button>
                  </Left>

                  <Right>

                    {
                      status == true ?

                        <Button
                          rounded
                          style={{
                            backgroundColor: '#fcf003',
                            width: Dimensions.get('window').width * 0.15,
                            justifyContent: 'center',
                          }}
                          onPress={() => this.updateDataCanceled(todo._id)}>
                          <Text
                            style={{
                              color: '#ffffff',
                              fontWeight: 'bold',
                              fontSize: 15
                            }}
                          >Cancel</Text>
                        </Button>

                        :

                        <Button
                          rounded
                          style={{
                            backgroundColor: '#35fc03',
                            width: Dimensions.get('window').width * 0.15,
                            justifyContent: 'center',
                          }}
                          onPress={() => this.updateData(todo._id)}>
                          <Text
                            style={{
                              color: '#ffffff',
                              fontWeight: 'bold',
                              fontSize: 15
                            }}
                          >Done</Text>
                        </Button>
                    }

                  </Right>

                </View>
              </Body>
            </CardItem>
          </Card>

        </View>
      );
    })

    return (
      <Container style={{ backgroundColor: '#e6e5e3' }}>


        <Content>

          <Header style={{ backgroundColor: '#00acee' }}
            androidStatusBarColor="#00acee">
            
            <Body>
              <Title style={styles.titleheader}>ToDoList</Title>
            </Body>

          </Header>


          {loops}


        </Content>


        <Fab
          style={{
            backgroundColor: '#22cade',
          }}
          onPress={() => this.props.navigation.navigate('AddToDoList')}>
          <Icon name="add" />
        </Fab>


      </Container>
    )
  }
}
export default withNavigation(ToDoListScreen);

const styles = StyleSheet.create({
  titleheader: {
    fontWeight: 'bold',
    alignItems: 'center',
    justifyContent: 'center',
  },
  signbutton: {
    fontWeight: 'bold'
  },
  button: {
    display: "flex",
    borderColor: 'transparent',
    margin: 10,
    justifyContent: "center",
    width: 2000,
  },
  text: {
    color: '#00acee',
    fontSize: 20,
    justifyContent: "center",
    alignContent: "center",
    fontWeight: 'bold'
  },
  buttonsignin: {
    margin: 10,
  }
})