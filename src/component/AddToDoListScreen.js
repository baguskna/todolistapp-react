import React, { Component } from 'react'
import { Text, View, ToastAndroid, Image, Dimensions } from 'react-native'
import { withNavigation } from 'react-navigation'
import AsyncStorage from '@react-native-community/async-storage'
import Axios from 'axios'
import { Container, Header, Left, Button, Body, Title, Right, Icon, Input, Item, Picker, DatePicker } from 'native-base'

class AddToDoListScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: '',
      name: '',
      title: '',
      notes: '',
      dueDate: '',
      priority: '',
      status: false,
      isLoading: false,
      isDateTimePickerVisible: false,
      chosenDate: new Date()
    }
    this.setDate = this.setDate.bind(this);
  }

  setDate(newDate) {
    this.setState({ chosenDate: newDate });
  }

  // componentDidMount() {
  //   this.getProfile()
  // }

  // getProfile = async () => {
  //   const token = await AsyncStorage.getItem('@token')
  //   const getData = async () => await Axios.get('https://app-todoapps.herokuapp.com/users/profile', {
  //     headers: {
  //       'Content-Type': 'application/json',
  //       'Authorization': `Bearer ${token}`
  //     }
  //   })
  //   getData()
  //     .then(res => {
  //       this.setState({
  //         data: res.data.result,
  //         name: res.data.result.name,
  //       })
  //     })
  // }

  addToDo = async () => {
    this.setState({
      isLoading: true
    })
    const token = await AsyncStorage.getItem('@token')
    const addData = async (bodyParam) => await Axios.post('https://app-todoapps.herokuapp.com/todos', bodyParam, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      }
    })
    addData({
      title: this.state.title,
      notes: this.state.notes,
      dueDate: this.state.chosenDate,
      status: this.state.status,
      priority: this.state.priority
    })
      .then(res => {
        this.setState({
          isLoading: false,
          title: '',
          notes: '',
          dueDate: '',
          status: '',
          priority: '',
        })
        ToastAndroid.show(res.data.message, ToastAndroid.SHORT)
      })
      .catch(() => {
        ToastAndroid.show('Failed to add todo', ToastAndroid.SHORT)
      })
  }


  onValueChange = (value) => {
    this.setState({
      status: value
    });
  };


  render() {
    return (
      <Container>


        <Header style={{ backgroundColor: '#ffffff' }}
          androidStatusBarColor="#00acee">

          <Left>
            <Button transparent
              onPress={() => { this.props.navigation.goBack() }}>
              <Icon name='close' style={{ color: '#00acee' }} />
            </Button>
          </Left>

          <Body>
            <Title style={{ color: '#00acee' }}>Add Todo</Title>
          </Body>

          <Right>
            <Button
              rounded
              style={{
                backgroundColor: '#00acee',
                width: Dimensions.get('window').width * 0.2,
                justifyContent: 'center',
                height: 35
              }}
              onPress={() => this.addToDo()}
            >
              <Text
                style={{
                  color: '#ffffff',
                  fontWeight: 'bold',
                  fontSize: 18
                }}
              >Add</Text>
            </Button>
          </Right>

        </Header>

        <View>

          <Item
            fixedLabel
            style={{
              width: Dimensions.get('window').width * 5 / 5,
              borderColor: '#a5a5af'
            }}
          >

            <Image
              style={{
                width: 45,
                height: 45,
                borderRadius: 45 / 2,
                marginHorizontal: 20,
                marginVertical: 20
              }}
              source={require('../photo/photo.png')} />

            <Input
              placeholder='Title...'
              placeholderTextColor='#a5a5af'
              value={this.state.title}
              multiline={true}
              onChangeText={(text) => this.setState({ title: text })}
              style={{ color: '#000000' }}
            />

          </Item>

          <Item
            fixedLabel
            style={{
              width: Dimensions.get('window').width * 5 / 5,
              borderColor: '#a5a5af',
            }}
          >
            <Input
              placeholder='Write a caption...'
              placeholderTextColor='#a5a5af'
              value={this.state.notes}
              multiline={true}
              onChangeText={(text) => this.setState({ notes: text })}
              style={{ color: '#000000', height: 120, marginHorizontal: 20, marginVertical: 20 }}
            />
          </Item>

          {/* <Item
            fixedLabel
            style={{
              width: Dimensions.get('window').width * 1,
              borderColor: '#a5a5af'
            }}
            >
              <Input 
              placeholder='Date'
              />
          </Item> */}
          <View
            style={{
              marginLeft: 10
            }}
          >

            <DatePicker
              defaultDate={new Date(2019, 10, 10)}
              minimumDate={new Date(2019, 1, 1)}
              maximumDate={new Date(2020, 12, 12)}
              locale={"en"}
              timeZoneOffsetInMinutes={undefined}
              modalTransparent={false}
              animationType={"fade"}
              androidMode={"default"}
              placeHolderText="Select Date"
              textStyle={{ color: 'green' }}
              placeHolderTextStyle={{ color: '#d3d3d3' }}
              onDateChange={this.setDate}
              disabled={false}
            />

            <Text>Date : {this.state.chosenDate.toString().substr(4, 12)}</Text>


          </View>

          <View style={{ marginTop: 10, marginLeft: 10 }}>

            <Text>Priority : </Text>

            <Picker
              mode="dropdown"
              iosHeader="Choose your status"
              iosIcon={<Icon name="arrow-down" />}
              selectedValue={this.state.status}
              onValueChange={this.onValueChange.bind(this)}
            >

              <Picker.Item label="Low" value="Low" />
              <Picker.Item label="Medium" value="Medium" />
              <Picker.Item label="High" value="High" />

            </Picker>
          </View>

        </View>


      </Container>
    )
  }
}
export default withNavigation(AddToDoListScreen);